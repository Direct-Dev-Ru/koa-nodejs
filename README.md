# Getting Started with Boilerplate

This project was made as full stack web app boilerplate.
It runs several Docker containers 

## Available Scripts

In the project directory, you can run:

### `yarm mail-koa-dev`

Runs the app in development mode.

## `Other usefull commands`

### `docker exec -it [CONTAINER_NAME] sh`

Then app running it opens specified container shell.