const path = require('path');
const { rootPath } = require('../../root');
const { readFile } = require('fs/promises');
const { readFileSync } = require('fs');

const readSecretSync = (filePath = '') => {
   let secretFilePath = filePath;
   if (!secretFilePath) {
      secretFilePath = path.resolve(rootPath, 'src', 'secrets', 'server.key');
   }
   const result = { value: '', error: null };
   try {
      const secret = readFileSync(secretFilePath, { encoding: 'utf8' }) || process.env.SERVER_KEY;

      result.value = secret;
   } catch (error) {
      if (process.env.SERVER_KEY) {
         result.value = process.env.SERVER_KEY;
      } else {
         result.error = error;
      }
   }
   return result;
};

exports.readSecretSync = readSecretSync;

const readSecret = async function (secretToSet) {
   try {
      const controller = new AbortController();
      const { signal } = controller;
      const secretFilePath = path.resolve(rootPath, 'src', 'secrets', 'server.key');
      //   console.log(secretFilePath, rootPath);
      const promise = readFile(secretFilePath, { signal });

      // Abort the request before the promise settles.
      // controller.abort();

      const buffer = await promise;
      secretToSet.value = buffer.toString('utf8') || process.env.SERVER_KEY;
      return secretToSet;
   } catch (err) {
      // When a request is aborted - err is an AbortError
      console.error(err);
   }
};
