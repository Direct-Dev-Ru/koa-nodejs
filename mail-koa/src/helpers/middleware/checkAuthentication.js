const { gp } = global.app;
const { logga } = global.app.ga;
const jwt = require('jsonwebtoken');
const config = require(gp.r('configuration'));
const key = config['TOKEN_KEY'];

const checkAuthentication = async function (ctx, next) {
   //    logga(ctx.isAuthenticated());
   if (ctx.isAuthenticated()) {
      const token = ctx.cookies.get('usertoken');
      logga(`--------working ${this.name}----------`);
      //   logga(['От клиента пришел токен: ', token]);
      let userSession = ctx.state.user;

      //   logga('user session:');
      //   logga(userSession, 'json');

      try {
         userCookie = jwt.verify(token, key);
         ctx.userCookie = userCookie;
         //  logga('user from Cookie:');
         //  logga(userCookie, 'json');

         if ((!userCookie || !userSession) && userSession['_id'] !== userCookie.user['id']) {
            console.log(
               'LOGOUT FAIL checkAuth:',
               userSession['_id'] !== userCookie['_id'],
               userSession['_id'],
               userCookie['_id']
            );
            ctx.throw(401, 'logout');
         }
      } catch (err) {
         // err
         ctx.throw(401, 'logout');
      }

      await next();
   } else {
      ctx.throw(401, 'logout');
   }
};

exports.checkAuthentication = checkAuthentication.bind(checkAuthentication);
