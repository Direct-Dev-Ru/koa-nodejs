const validateObjectId = (ctx, next) => {
   if (!db.mongoose.Types.ObjectId.isValid(ctx.params.id)) {
      ctx.throw(400, `Not found: invalid id ${ctx.params.id} not found`);
      return;
   }

   return next();
};

exports.validateObjectid = validateObjectId;
