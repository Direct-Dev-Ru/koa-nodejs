const handleValidationError = async function handleValidationError(ctx, next) {
   try {
      await next();
   } catch (err) {
      if (err.name !== 'ValidationError') {
         throw err;
      }
      ctx.status = 400;
      console.log(JSON.stringify(err, null, 4));
      const errmessage = Object.keys(err.errors)
         .map((key) => `${key}: ${err.errors[key].message}`)
         .join(', ');
      ctx.body = {
         isError: true,
         message: {
            message: 'Validation error: ' + errmessage,
            stack: err.stack,
         },
         errorCode: 400,
      };
   }
};

exports.handleValidationError = handleValidationError;
