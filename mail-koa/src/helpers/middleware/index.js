const { validateObjectId } = require('./validateObjectId');
const { handleValidationError } = require('./handleValidationError');
const { checkAuthentication } = require('./checkAuthentication');

module.exports = {
   validateObjectId,
   handleValidationError,
   checkAuthentication,
};
