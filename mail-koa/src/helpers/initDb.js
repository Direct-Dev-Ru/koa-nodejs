module.exports.initDb = async (db, collection, drop = true, documents) => {
   if (drop) {
      console.log('dropping db collection ' + collection);
      await db[collection].deleteMany();
   }
   try {
      const collectionDocuments = await Promise.all(
         documents[collection].map(async (doc) => {
            const docToDb = {};

            for (const key in doc) {
               if (key.startsWith('for_')) {
                  keyDb = key.replace('for_', '');
                  const mapObject = doc[key];
                  // foreign request
                  const foreign = await db[mapObject.collection].find().lean();
                  if (mapObject.all) {
                     docToDb[keyDb] = foreign.map((el) => el._id);
                  } else {
                     docToDb[keyDb] = foreign
                        .filter((el) => {
                           const searchField = el[mapObject.key];
                           return mapObject.documents.includes(searchField);
                        })
                        .map((el) => el._id);
                  }
               } else {
                  docToDb[key] = doc[key];
               }
            }
            return docToDb;
         })
      );

      console.log('to db collection:', collectionDocuments);

      const result = await Promise.all(
         collectionDocuments.map(async (document) => {
            return await db[collection].create(document);
         })
      );
      return result;
   } catch (err) {
      console.log(err);
      await db[collection].deleteMany();
      return [];
   }
};
