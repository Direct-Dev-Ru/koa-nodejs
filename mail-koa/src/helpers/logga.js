exports.logga = (logs, type = 'log') => {
   // console.log(process.env.NODE_ENV,(process.env.NODE_ENV === "production"));
   if (!(process.env.NODE_ENV === 'production')) {
      let output = logs;
      let _type = type;
      if (type === 'json') {
         output = JSON.stringify(logs, null, 4);
         _type = 'log';
      }

      console[_type](output);
   }
};
