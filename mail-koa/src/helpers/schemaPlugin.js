// TODO investigate: is here needed async await
module.exports = function (schema) {
   const updateDate = async function (next) {
      const self = this;
      self.updatedAt = new Date();
      if (!self.createdAt) {
         self.createdAt = self.updatedAt;
      }
      await next();
   };
   // update date for bellow 4 methods
   schema
      .pre('save', updateDate)
      .pre('update', updateDate)
      .pre('findOneAndUpdate', updateDate)
      .pre('findByIdAndUpdate', updateDate);
};
