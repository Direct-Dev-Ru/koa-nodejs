// const mongoose = require('mongoose')
const { DB, connectionStrWoPwd } = require('../configuration');
const { logga } = require('./logga');

module.exports.connectDb = (
   db,
   options = {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
      autoIndex: true, // Don't build indexes
      poolSize: 10, // Maintain up to 10 socket connections
      // serverSelectionTimeoutMS: 5000, // Keep trying to send operations for 5 seconds
      // socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
      family: 4, // Use IPv4, skip trying IPv6
   }
) => {
   logga('Trying connecting to mongo db: ' + connectionStrWoPwd);
   db.mongoose.connect(DB, options);
   return db.mongoose.connection;
};
