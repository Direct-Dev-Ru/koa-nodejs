const path = require('path');
const { rootPath } = require('../../root');
const { readSecretSync } = require('./getSecret');
const { readFileSync, writeFileSync } = require('fs');
const jwt = require('jsonwebtoken');

const encryptJson = (object) => {
   const secret = { value: null };
   let crypto;
   let resultObject = {};
   try {
      crypto = require('crypto');
      try {
         const { value: secret } = readSecretSync();

         for (const objkey in object) {
            if (Object.hasOwnProperty.call(object, objkey)) {
               const element = object[objkey];
               let plaintext = '*string*' + element.toString();

               if (typeof element === 'object') {
                  plaintext = '*object*' + JSON.stringify(element);
               } else if (typeof element === 'number') {
                  plaintext = '*number*' + JSON.stringify(element);
               }

               const key = secret.substr(0, 24);
               const nonce = crypto.randomBytes(12);
               const aad = Buffer.from('dgfhytdgfh', 'hex');
               const cipher = crypto.createCipheriv('aes-192-ccm', key, nonce, {
                  authTagLength: 16,
               });

               cipher.setAAD(aad, {
                  plaintextLength: Buffer.byteLength(plaintext),
               });
               const ciphertext = cipher.update(plaintext, 'utf8');
               cipher.final();
               const tag = cipher.getAuthTag();

               resultObject[objkey] = JSON.stringify({
                  ciphertext: ciphertext,
                  nonce: nonce,
                  tag: tag,
               });
            }
         }
         return resultObject;
      } catch (error) {
         return { error };
      }
   } catch (error) {
      return { error };
   }
};

// const encObject = encryptJson({ v1: 'Валюта', v2: 'Super Secret 2', v3: 33 });
// console.log(encObject);

const decryptJson = (encObject) => {
   const secret = { value: null };
   let crypto;
   let resultObject = {};
   try {
      crypto = require('crypto');
      try {
         const { value: secret } = readSecretSync();

         for (const objKey in encObject) {
            if (Object.hasOwnProperty.call(encObject, objKey)) {
               const element = encObject[objKey];
               const enc = JSON.parse(element);

               const key = secret.substr(0, 24);
               const ciphertext = Buffer.from(enc.ciphertext.data);
               const nonce = Buffer.from(enc.nonce.data);
               const tag = Buffer.from(enc.tag.data);

               const aad = Buffer.from('dgfhytdgfh', 'hex');

               const decipher = crypto.createDecipheriv('aes-192-ccm', key, nonce, {
                  authTagLength: 16,
               });
               decipher.setAuthTag(tag);
               decipher.setAAD(aad, {
                  plaintextLength: ciphertext.length,
               });
               const receivedPlaintext = decipher.update(ciphertext, null, 'utf8');

               try {
                  decipher.final();

                  const type = receivedPlaintext.substring(0, 8);

                  switch (type) {
                     case '*object*':
                        resultObject[objKey] = JSON.parse(receivedPlaintext.substring(8, 100000000));
                        break;
                     case '*number*':
                        resultObject[objKey] = +receivedPlaintext.substring(8, 100000000);
                        break;
                     default:
                        resultObject[objKey] = receivedPlaintext.substring(8, 100000000);
                        break;
                  }
               } catch (err) {
                  throw new Error('decryption failed!', { cause: err });
               }
            }
         }
         return resultObject;
      } catch (error) {
         return { error };
      }
   } catch (err) {
      return { error };
   }
};

// const decObject = decryptJson(encObject);
// console.log(decObject);

const writeEncrypted = (filename, obj, secretKey) => {
   let secret = secretKey;
   try {
      if (!secret) {
         secret = readSecretSync().value;
      }
      let data = JSON.stringify(encryptJson(obj));
      //    let buff = new Buffer.from(data);
      //    let base64data = buff.toString('base64');
      const token = jwt.sign(data, secret);

      const pathToWrite =
         filename.split(path.delimiter).length > 1 ? filename : path.resolve(rootPath, 'src', 'secrets', filename);
      console.log(pathToWrite);
      writeFileSync(pathToWrite, token);
      return { token, data };
   } catch (error) {
      
      return { error };
   }
};

const readEncrypted = (filename, secretKey) => {
   let secret = secretKey;
   if (!secret) {
      secret = readSecretSync().value;
   }

   try {
      const pathToRead =
         filename.split(path.delimiter).length > 1 ? filename : path.resolve(rootPath, 'src', 'secrets', filename);

      const token = readFileSync(pathToRead).toString('utf-8');
      const objToDecode = jwt.verify(token, secret);
      const decObject = decryptJson(objToDecode);
      return decObject;
   } catch (error) {
      return { error };
   }
};

module.exports = { encryptJson, decryptJson, writeEncrypted, readEncrypted };
