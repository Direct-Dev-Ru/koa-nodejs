const { settings } = require('../secrets/secSettings.js');
const { encryptJson, decryptJson, writeEncrypted, readEncrypted } = require('./cypher');

const dummy = { username: 'dummy@gmail.com', pwd: '123456qwerty', type: 'email' };

const testKey = 'TestirovanieEtoNasheVse!!!!';

console.log('befor encrypting:', dummy);
try {
    encrypted = writeEncrypted('encTestConfig.enc', dummy, testKey);
 } catch (error) {
    encrypted = { error };
 }
 console.log('encrypted', encrypted);

let decrypted = {};
try {
   decrypted = readEncrypted('encTestConfig.enc', testKey);
} catch (error) {
   decrypted = { error };
}
console.log('after decrypted', decrypted);
