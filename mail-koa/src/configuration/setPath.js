const Path = require('path');
const baseDefault = Path.resolve(require('../../root').rootPath, 'src');

const paths = [
   { name: 'configuration', path: 'configuration' },
   { name: 'handlers', path: 'handlers' },
   { name: 'secrets', path: 'secrets' },
   { name: 'helpers', path: 'helpers' },
   { name: 'libs', path: 'libs' },
   { name: 'libs-passp', path: `libs/pasport` },
   { name: 'libs-passp-strat', path: `libs/pasport/strategies` },
   { name: 'models', path: `models` },
   { name: 'public', path: `public` },
   { name: 'routes', path: `routes` },
   { name: 'routes-opened', path: `routes/opened` },
   { name: 'routes-closed', path: `routes/closed` },
];

const makePaths = (base = baseDefault, _paths = paths) => {
   const result = {};
   for (const path of _paths) {
      const pathArray = path.path.split('/');
      result[path.name] = Path.resolve(base, ...pathArray);
   }

   result.resolve = function (key, restPath) {
      if (restPath) {
         return Path.resolve(this[key], ...restPath.split('/'));
      }
      return Path.resolve(this[key]);
   };
   result.r = result.resolve;
   result.base = base;

   return result;
};

const groupRequest = (base = baseDefault, _paths = paths) => {
   const modules = {};
   for (const path of _paths) {
      const pathArray = path.path.split('/');
      try {
         modules[path.name] = request(Path.resolve(base, ...pathArray));
      } catch (err) {
         modules[path.name] = () => {
            console.error(err);
         };
      }
   }
};

exports.makePaths = makePaths;
exports.groupRequest = groupRequest;
exports.p = makePaths();

// module.exports = pathAliases;
