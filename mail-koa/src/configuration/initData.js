module.exports = {
   mailRecipients: [
      {
         email: 'kuzant24@gmail.com',
         fullname: 'Кузнецов Антон Юрьевич',
      },
      {
         email: 'prasolovaen@yandex.ru',
         fullname: 'Кузнецова Елена Николаевна',
      },
      {
         email: 'someuser@yandex.ru',
         fullname: 'Иванов Андрей Ваганович',
      },
      {
         email: 'someuser2@yandex.ru',
         fullname: 'Сидоров Петр Петрович',
      },
   ],

   newsletters: [
      {
         name: 'Main Newsletter',
         description: 'Main newsletter to send all registered users by default',
         periodInSeconds: 3600,
         for_members: {
            collection: 'mailRecipients',
            all: true,
            key: 'email',
            documents: [],
         },
      },
      {
         name: 'New Posts in IT blog',
         description: 'Newsletter about IT posts',
         periodInSeconds: 3600,
         for_members: {
            collection: 'mailRecipients',
            all: false,
            key: 'email',
            documents: ['kuzant24@gmail.com', 'someuser@yandex.ru'],
         },
      },
   ],
};
