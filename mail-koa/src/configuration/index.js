const path = require('path');
const { p } = require('./setPath');
const { logga } = require(p.resolve('helpers', 'logga'));
const initData = require('./initData');

const cypher = require(p.resolve('helpers', 'cypher'));

const dropDb = process.env.DROP || 'false';
const templatesRoot = path.join(process.cwd(), 'src', 'templates');
const DB = process.env.MONGO_URL;
let connectionStrWoPwd = DB.substring(DB.indexOf(`//`), DB.indexOf(`@`));
connectionStrWoPwd =
   DB.substring(0, DB.indexOf(`//`)) +
   connectionStrWoPwd.substring(0, connectionStrWoPwd.indexOf(`:`) + 1) +
   `*********` +
   DB.substring(DB.indexOf(`@`), 1000);

let secConfig = {};
try {
   secConfig = cypher.readEncrypted('encConfig.enc');
} catch (error) {
   console.error(error);
   process.exit(1);
}

openConfig = {
   p,
   PORT: process.env.PORT,
   HOST: process.env.HOST,
   DB,
   connectionStrWoPwd,
   DROP: JSON.parse(dropDb.toLowerCase().trim()),
   DATA_API_BASEURL: process.env.DATA_API_BASEURL,
   AUTH_API_BASEURL: process.env.AUTH_API_BASEURL,
   INIT_DATA: initData,
   SESSION_KEYS: process.env.SESSION_KEYS || 'fsdfsdfsdmdfweuinusdnwujncdfsadfijg45t8tgfdjfivn',
   TOKEN_KEY:
      process.env.TOKEN_KEY ||
      '94ut3feigjreiq3945ufktuihguier092343u583cftm87thm23vtg9j324uyhcfghijcfkwqeopr340rtk90134t',
   EXPIRATION: 7 * 24 * 60 * 60 * 1000,
   TEMPLATES_ROOT: templatesRoot,
   providers: {
      facebook: {
         appId: 'xxxxxxxxxxxxxxxxxxxxxxxxxx',
         appSecret: 'xxxxxxxxxxxxxxxxxxxxxxxxx',
         passportOptions: {
            scope: ['email'],
         },
      },
      vk: {
         appId: '7993543',
         appSecret: 'AXFYzHfzgMYth2FV8ozb',
         callbackURL: `http://localhost:3003/oauth/vkontakte`,
         passportOptions: {
            scope: ['email'],
         },
      },
      github: {
         clientID: '8666a07862c7337da475',
         clientSecret: 'df320c5e7bbeb6d74e079f0d2f8f9b64a6e6162c',
         callbackURL: `http://localhost:3003/oauth/github`,
         passportOptions: {
            scope: ['email'],
         },
      },
   },
};

config = Object.assign({}, openConfig, secConfig);

module.exports = config;
