const mongoose = require('mongoose');
const mongooseUniquePlugin = require('mongoose-beautiful-unique-validation');
mongoose.set('debug', true);
mongoose.plugin(mongooseUniquePlugin);
mongoose.Promise = global.Promise;

const db = {};

db.mongoose = mongoose;

db.config = require('../configuration');
db.helpers = require('../helpers');

module.exports = db;
