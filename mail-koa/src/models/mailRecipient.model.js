const { mongoose } = require('./db');
const schemePlugin = require('../helpers/schemaPlugin');

const mailRecipientsSchema = new mongoose.Schema(
   {
      email: {
         type: String, // тип: String
         required: [true, 'emailRequired'],
         maxlength: [256, 'emailTooLong'],
         validate: [
            {
               validator: (value) => {
                  return /[A-Z0-9._%+-]+@[A-Z0-9-]+.+\.[A-Z]{2,4}/gim.test(value);
               },
               message: 'Incorrect email',
            },
         ],
         unique: 'This email already presented in db',
         lowercase: true,
         trim: true,
      },
      fullname: {
         type: String,
      },
      createdAt: {
         type: Date,
      },
      updatedAt: {
         type: Date,
      },
   },
   { toJSON: { virtuals: true } },
   { toObject: { virtuals: true } }
);

mailRecipientsSchema.virtual('newsletters', {
   ref: 'Newsletter',
   localField: '_id',
   foreignField: 'members',
});

mailRecipientsSchema.index({ email: 1, fullname: 1 });
mailRecipientsSchema.plugin(schemePlugin);

const MailRecipient = mongoose.model('MailRecepient', mailRecipientsSchema);

module.exports = MailRecipient;
