const { mongoose } = require('./db');
const schemePlugin = require('../helpers/schemaPlugin');

const newsletterSchema = new mongoose.Schema({
   name: {
      type: String, // тип: String
      required: [true, 'Newsletter name required'],
      maxlength: [256, 'name TooLong'],
      unique: 'This newsletter already presented in db',
      lowercase: true,
      trim: true,
      index: true,
   },
   description: {
      type: String,
   },
   startedAt: {
      type: Date,
      default: Date.now(),
   },
   periodInSeconds: {
      type: Number,
   },
   members: [{ type: mongoose.Schema.Types.ObjectId, ref: 'MailRecepient' }],
   createdAt: {
      type: Date,
   },
   updatedAt: {
      type: Date,
   },
});

newsletterSchema.plugin(schemePlugin);

const Newsletter = mongoose.model('Newsletter', newsletterSchema);

module.exports = Newsletter;
