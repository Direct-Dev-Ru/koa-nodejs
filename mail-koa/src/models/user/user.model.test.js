const { userSchema } = require('./user.model');
const { roleSchema } = require('./role.model');
const config = require('../../configuration/testConfig');
const mongoose = require('mongoose');

const mapUser = (user) => {
   return {
      id: user._id,
      displayname: user.displayname,
      email: user.email,
      roles: user.roles,
      pwd: user.passwordHash,
      salt: user.salt,
      createdAt: user.createdAt,
      updatedAt: user.updatedAt,
   };
};

const connection = mongoose.createConnection(config.DB_URL, {
   useNewUrlParser: true,
   useUnifiedTopology: true,
   useCreateIndex: true,
});

let User = {};
let Role = {};

connection.on('connected', () => {
   console.log('connected to mongodb');

   User = connection.model('User', userSchema, 'users');
   Role = connection.model('Role', roleSchema, 'roles');
   testUsers()
      .catch(console.log)
      .then(() => mongoose.disconnect());
});

connection.on('disconnected', () => {
   console.log('connection disconnected');
});

async function testUsers() {
   await User.deleteMany();
   await Role.deleteMany();

   const initUsers = [
      {
         email: 'info@direct-dev.ru',
         displayname: 'adminka',
         password: '123123',
      },
      // {
      // 	email: "main-mailer@direct-dev.ru",
      // 	displayname: "main-mailer",
      // 	password: "123123",
      // },
      // {
      // 	email: "power-user@direct-dev.ru",
      // 	displayname: "power-user",
      // 	password: "222333",
      // },
      // {
      // 	email: "dbreader1@direct-dev.ru",
      // 	displayname: "database-reader1",
      // 	password: "345555",
      // },
      {
         email: 'dbreader2@direct-dev.ru',
         displayname: 'database-reader2',
         password: '423423423dx',
      },
   ];

   const newusers = [];

   for (const initUser of initUsers) {
      const newUser = new User(initUser);
      await newUser.setPassword(initUser.password);
      await newUser.save();
      newusers.push(newUser);
   }

   const newroles = [];
   newroles[0] = await Role.create({
      rolename: 'admin',
      members: [newusers[0]],
   });
   newroles[1] = await Role.create({
      rolename: 'poweruser',
      members: [newusers[2], newusers[0]],
   });
   newroles[2] = await Role.create({
      rolename: 'user',
      members: [newusers[1]],
   });
   newroles[3] = await Role.create({
      rolename: 'reader',
      members: [newusers[3], newusers[4]],
   });

   const findedUsers = await User.find({
      email: 'info@direct-dev.ru',
   }).populate('roles');
   // const findedUsers = await User.find({}).populate("roles").sort({ name: 1 });
   console.log(JSON.stringify(findedUsers.map(mapUser), null, 2));
}
