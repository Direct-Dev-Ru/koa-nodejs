const { mongoose } = require('../db');
const crypto = require('crypto');
const schemePlugin = require('../../helpers/schemaPlugin');

const userSchema = new mongoose.Schema(
  {
    email: {
      type: String, // тип: String
      required: [true, 'Email is required field'],
      maxlength: [256, 'Email is too Long'],
      validate: [
        {
          validator: (value) => {
            return /[A-Z0-9._%+-]+@[A-Z0-9-]+.+\.[A-Z]{2,4}/gim.test(value);
          },
          message: 'Incorrect email',
        },
      ],
      unique: 'This email already presented in db',
      lowercase: true,
      trim: true,
    },
    displayname: {
      type: String,
      required: [true, 'User must have a name'],
      index: true,
    },
    passwordHash: {
      type: String,
    },
    salt: {
      type: String,
    },
    roles: { type: [String], default: ['everyone'] },
    provider: {
      type: String,
    },
    providers: [
      {
        id: String,
        profile: {},
      },
    ],
  },
  { timestamps: true },
  { toJSON: { virtuals: true } },
  { toObject: { virtuals: true } }
);

// userSchema.virtual('roles', {
//   ref: 'Role',
//   localField: '_id',
//   foreignField: 'members',
// });

userSchema.index({ email: 1, name: 1 });

// password functions
const defaultCryptoOptions = { iterations: 5, length: 256, digest: 'sha512' };

function generatePassword(password, salt, options = { ...defaultCryptoOptions }) {
  return new Promise((resolve, reject) => {
    crypto.pbkdf2(password, salt, options.iterations, options.length, options.digest, (err, key) => {
      if (err) {
        return reject(err);
      }
      resolve(key.toString('hex'));
    });
  });
}

function generateSalt(options = { ...defaultCryptoOptions }) {
  return new Promise((resolve, reject) => {
    crypto.randomBytes(options.length, (err, buffer) => {
      if (err) {
        return reject(err);
      }
      resolve(buffer.toString('hex'));
    });
  });
}

userSchema.methods.setPassword = async function (password, options = { ...defaultCryptoOptions }) {
  this.salt = await generateSalt();
  this.passwordHash = await generatePassword(password, this.salt, options);
};

userSchema.methods.checkPassword = async function (password, options = { ...defaultCryptoOptions }) {
  if (!password) {
    return false;
  }
  const hash = await generatePassword(password, this.salt, options);
  return hash === this.passwordHash;
};

// end of password functions

// helper methods

// helper functions

const validateObjectId = (ctx, next) => {
  if (!db.mongoose.Types.ObjectId.isValid(ctx.params.id)) {
    ctx.throw(400, `Not found: invalid id ${ctx.params.id} not found`);
    return;
  }

  return next();
};

const mainMapUser = (user) => {
  return {
    id: user._id,
    displayname: user.displayname,
    email: user.email,
    roles: user.roles,
    providers: user.providers,
    provider: user.provider,
    createdAt: user.createdAt,
    updatedAt: user.updatedAt,
  };
};

userSchema.methods.mapUser = function (type = 'main', user) {
  const selector = {
    main: () => {
      return mainMapUser(user || this);
    },
  };
  return selector[type]();
};
userSchema.mapUser = userSchema.methods.mapUser;
// end of helper methods

const User = mongoose.model('User', userSchema); //collection will be users
// const User = mongoose.model("User", userSchema, 'polzovately'); //collection will be polzovately

module.exports = { userSchema, User };
