const { mongoose } = require('../db');
const schemePlugin = require('../../helpers/schemaPlugin');

const roleSchema = new mongoose.Schema(
   {
      rolename: {
         type: String, // тип: String
         required: [true, 'roleNameRequired'],
         maxlength: [256, 'roleNameTooLong'],
         unique: 'This rolename already presented in db',
         lowercase: true,
         trim: true,
      },
      members: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
   },
   { timestamps: true }
);

// roleSchema.index({ rolename: 1 });

// roleSchema.plugin(schemePlugin);

const Role = mongoose.model('Role', roleSchema); //collection will be roles

module.exports = { roleSchema, Role };
