const db = require('./db');

db.config = require('../configuration');
db.helpers = require('../helpers');

db.mailRecipients = require('./mailRecipient.model');
db.newsletters = require('./newsletter.model');
db.users = require('./user').User;
db.roles = require('./user').Role;

module.exports = db;
