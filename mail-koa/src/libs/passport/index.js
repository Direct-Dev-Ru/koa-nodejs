const passport = require('koa-passport');
const User = require('../../models/index').users;

const localStrategy = require('./strategies/local');
const vkStrategy = require('./strategies/vk');
const githubStrategy = require('./strategies/github');

passport.serializeUser(function (user, done) {
   done(null, user.id);
});

passport.deserializeUser(function (id, done) {
   User.findById(id, done);
});

passport.use(localStrategy);
passport.use(vkStrategy);
passport.use(githubStrategy);

module.exports = passport;
