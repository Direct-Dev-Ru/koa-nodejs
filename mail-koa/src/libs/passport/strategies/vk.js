const { gp } = global.app;
const models = require(gp.r('models'));
const config = require(gp.r('configuration'));

const VKStrategy = require('passport-vkontakte').Strategy;
const User = models.users;

module.exports = new VKStrategy(
   {
      clientID: config.providers.vk.appId,
      clientSecret: config.providers.vk.appSecret,
      callbackURL: config.providers.vk.callbackURL,
      scope: ['email'],
      profileFields: ['email'],
   },
   function (accessToken, refreshToken, params, profile, done) {
      const email = params.email;

      User.findOne({ email }, (err, user) => {
         if (err) return done(err);

         if (!user) {
            User.create(
               {
                  email,
                  displayname: profile.displayName,
                  provider: 'vk',
                  providers: [{ id: 'vk', profile }],
               },
               (err, user) => {
                  if (err) return done(err);
                  done(null, user, {
                     message: 'VKontakte knows you! Login success. You are welcome !!!',
                  });
               }
            );
         } else {
            if (user.providers.find((provider) => provider.id === 'vk')) {
               user.provider = 'vk';
               user.displayname = profile.displayName;
               done(null, user, {
                  message: 'VKontakte knows you! Login success. You are welcome !!!',
               });
            } else {
               user.providers.push({ id: 'vk', profile });
               user.provider = 'vk';
               user.displayname = profile.displayName;

               user.save((err) => {
                  if (err) return done(err);

                  done(null, user, {
                     message: 'VKontakte knows you! Login success. You are welcome !!!',
                  });
               });
            }
         }
      });
   }
);
