const LocalStrategy = require('passport-local');
const User = require('../../../models/index').users;

module.exports = new LocalStrategy(
   {
      usernameField: 'email',
      passwordField: 'password',
   },
   async function (email, password, done) {
      try {
         const user = await User.findOne({ email });
         if (!user) {
            return done(null, false, {
               message: 'No such user in database. Check email!!!',
            });
         }

         const isValidPassword = await user.checkPassword(password);

         if (!isValidPassword) {
            return done(null, false, { message: 'Wrong password!!!' });
         }

         user.provider = 'local';
         return done(null, user, {
            message: 'Login success. You are welcome.',
         });
      } catch (err) {
         console.error('From Local strategie', err);
         done(err);
      }
   }
);
