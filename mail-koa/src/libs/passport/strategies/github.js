const { gp } = global.app;
const { logga } = global.app.ga;
const models = require(gp.r('models'));
const config = require(gp.r('configuration'));

const GitHubStrategy = require('passport-github2').Strategy;
const User = models.users;

module.exports = new GitHubStrategy(
   {
      clientID: config.providers.github.clientID,
      clientSecret: config.providers.github.clientSecret,
      callbackURL: config.providers.github.callbackURL,
      scope: ['user:email'],
   },
   function (accessToken, refreshToken, params, profile, done) {
      logga(['params:', params]);
      logga(['profile:', profile], 'json');
      const emails = profile?.emails;
      if (emails.length === 0) {
         throw (401, "profile on github does't contain emails");
         return;
      }
      const email = emails[0].value;

      const profileDisplayName = profile.displayName || profile.username;
      User.findOne({ email }, (err, user) => {
         if (err) return done(err);

         if (!user) {
            User.create(
               {
                  email,
                  displayname: profileDisplayName,
                  provider: 'github',
                  providers: [{ id: 'github', profile }],
               },
               (err, user) => {
                  if (err) return done(err);
                  done(null, user, {
                     message: 'GitHub knows you! Login success. You are welcome !!!',
                  });
               }
            );
         } else {
            if (user.providers.find((provider) => provider.id === 'github')) {
               user.provider = 'github';
               user.displayname = profileDisplayName;
               done(null, user, {
                  message: 'GitHub knows you! Login success. You are welcome !!!',
               });
            } else {
               user.providers.push({ id: 'github', profile });
               user.provider = 'github';
               user.displayname = profileDisplayName;

               user.save((err) => {
                  if (err) return done(err);

                  done(null, user, {
                     message: 'GitHub knows you! Login success. You are welcome !!!',
                  });
               });
            }
         }
      });
   }
);
