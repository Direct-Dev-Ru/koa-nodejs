const pug = require('pug');
const path = require('path');
const { TEMPLATES_ROOT } = require('../configuration/');

exports.init = (app) =>
   app.use(async (ctx, next) => {
      /* default helpers */
      ctx.locals = {
         /* at the time of ctx middleware, user is unknown, so we make it a getter */
         get user() {
            return ctx.state.user; // passport sets ctx
         },

         get csrf() {
            return ctx.csrf;
         },

         get flash() {
            return ctx.getFlashMessages();
         },

         get mainContent() {
            return ctx.mainContent || { title: '', author: '', content: '' };
         },
      };

      ctx.render = function (templatePath, locals) {
        //  console.log(ctx.locals.csrf);
        //  console.log(ctx.locals.flash);
        //  console.log(ctx.locals.user);
         return pug.renderFile(path.join(TEMPLATES_ROOT, templatePath), Object.assign({}, ctx.locals, locals));
      };

      await next();
   });
