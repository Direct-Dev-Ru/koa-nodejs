const { logga } = require('../helpers/logga');

exports.init = (app) =>
   app.use(async (ctx, next) => {
      try {
         await next();
      } catch (err) {
         switch (true) {
            case ctx.url === 'api/auth/signin' || ctx.url.startsWith('/api/'):
               apiHandler(err, ctx);
               break;

            default:
               mvcDefault(err, ctx);
               break;
         }
      }
   });

const mvcDefault = (err, ctx) => {
   logga('ERROR INTERCEPT IN DEFAULT HANDLER:');
   
   logga(["ERROR STATUS: ", err.status]);
   logga(["ERROR MESSAGE: ", err.message]);
   logga(["ERROR STACK: ", err.stack]);

   if ((err.status = 401 && err.message === 'logout')) {
      logga(['ERROR:', err.message]);

      ctx.redirect('/logout');
      return;
   }

   if (err.status) {
      ctx.status = err.status;

      ctx.body = {
         isError: true,
         message: {
            message: err.message,
            stack: err.stack,
         },

         errorCode: err.status,
      };
      return;
   } else {
      // server error e.g. throw new Error ("Error thrown")
      logga(['ERROR WITHOUT CODE:', err.message, err.stack]);
      if (err.stack.toString().includes('UnauthorizedError')) {
         ctx.status = 200;
         ctx.body = {
            isError: true,
            message: {
               message: err.message,
               stack: err.stack,
            },
            errorCode: 0,
         };
         return;
      }
      ctx.status = 500;
      ctx.body = {
         isError: true,
         message: {
            message: 'Internal server error: ' + err.message,
            stack: err.stack,
         },
         errorCode: 500,
      };

      return;
   }
};

const apiHandler = (err, ctx) => {
   logga('ERROR INTERCEPT IN API HANDLER:');

   logga(["ERROR STATUS: ", err.status]);
   logga(["ERROR MESSAGE: ", err.message]);
   logga(["ERROR STACK: ", err.stack]);

   if (err.status) {
      ctx.status = 200;

      ctx.body = {
         isError: true,
         message: {
            message: err.message,
            stack: err.stack,
         },

         errorCode: err.status,
      };
      logga(["ctx.body: ", ctx.body]);
      return;
   } else {
      // server error e.g. throw new Error ("Error thrown")
      logga(['ERROR WITHOUT CODE']);
      if (err.stack.toString().includes('UnauthorizedError')) {
         ctx.status = 200;
         ctx.body = {
            isError: true,
            message: {
               message: err.message,
               stack: err.stack,
            },
            errorCode: 0,
         };
         return;
      }
      ctx.status = 200;
      ctx.body = {
         isError: true,
         message: {
            message: 'Internal server error: ' + err.message,
            stack: err.stack,
         },
         errorCode: 500,
      };

      return;
   }
};
