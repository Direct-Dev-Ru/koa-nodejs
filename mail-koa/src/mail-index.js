// Global settings start
const { p: gp } = require('./configuration/setPath');
const { logga } = require(gp.r('helpers'));

global.app = {};
global.app.gp = gp;

global.app.ga = {};
global.app.ga.logga = logga;
global.app.ga.mw = require(gp.r('helpers', 'middleware'));

global.app.config = {};
global.app.config = require(gp.r('configuration'));

// Init Koa
const http = require('http');
const https = require('https');
const Koa = require('koa');
const cors = require('@koa/cors');
// const cors = require('koa-cors');
const { csrf } = require(gp.r('helpers', 'middleware/csrf'));

const db = require('./models');

const app = new Koa();
const { PORT, HOST, DROP, DB, INIT_DATA, SESSION_KEYS } = global.app.config;
const keys = SESSION_KEYS.split(',');
app.context.config = global.app.config;
app.keys = [...keys];
app.context.db = db;

// logga(app.context.config);

require('./handlers/05-errors').init(app);

const corsOptions = {
   credentials: true,
   //    methods: 'GET,HEAD,PUT,POST,DELETE,PATCH,OPTIONS',
};
app.use(cors(corsOptions));

require('./handlers/01-favicon').init(app);
require('./handlers/02-static').init(app);
require('./handlers/03-logger').init(app);
require('./handlers/04-templates').init(app);
require('./handlers/06-session').init(app);
require('./handlers/07-bodyParser').init(app);

app.use(
   csrf({
      invalidStatusCode: 403,
      invalidTokenMessage: 'Invalid CSRF token',
      ignoreMethods: ['GET', 'HEAD', 'OPTIONS'],
      ignorePaths: ['/logout'],
      secretLength: 16,
      saltRounds: 10,
   })
);
require('./handlers/08-passport').init(app);
require('./handlers/09-flash').init(app);

const serverCb = async () => {
   if (false) {
      const initMailRecipients = await db.helpers.initDb(db, 'mailRecipients', DROP, INIT_DATA);
      const initNewsletters = await db.helpers.initDb(db, 'newsletters', DROP, INIT_DATA);
   }
};
const startServer = () => {
   http.createServer(app.callback()).listen(PORT, async () => {
      console.log(`Service started on ${HOST}:${PORT} ...`);
      await serverCb();
   });
   // https.createServer(app.callback()).listen(3001);
   // app.listen(PORT);
};

app.use(async (ctx, next) => {
   const now = new Date();
   await next();
   // logga(ctx.req);
   const ms = Date.now() - now;
   ctx.set('X-Response-Time', `${ms}ms`);
   logga(`NEW REQUEST: ${ctx.method}, Route: ${ctx.url}, Time: ${now}, Duration: ${ms}ms`);
});

// Routes includes

app.use(require(gp.routes).routes());
// app.use(require(gp.r("routes", "api")).routes());

// 404
app.use(async (ctx, next) => {
   ctx.status = 404;
   ctx.body = {
      isError: true,
      message: {
         message: 'Resource not found',
         stack: ctx.path,
      },

      errorCode: 404,
   };
   return;
});

db.helpers
   .connectDb(db)
   .on('error', (err) => {
      console.log('Mongoose default connection error: ' + err);
   })
   // .on("disconnected", db.helpers.connectDb)
   .on('disconnected', () => {
      console.log('Mongoose default connection disconnected');
   })
   .on('SIGINT', function () {
      db.mongoose.connection.close(function () {
         console.log('Mongoose default connection disconnected through app termination');
         process.exit(0);
      });
   })
   .once('open', () => {
      startServer();
   });
