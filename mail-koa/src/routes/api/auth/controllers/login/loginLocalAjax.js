const passport = require('../../../../../libs/passport');

async function loginLocalPassport(ctx, next) {
   await passport.authenticate('local', async function (err, user, info) {
      if (err) throw err;

      if (user) {
         await ctx.login(user);
         console.log(user);
         ctx.body = {
            isError: false,
            message: {
               isValid: true,
               message: info,
               user: {
                  id: user.id,
                  displayname: user.displayname,
                  email: user.email,
               },
            },
            errorCode: 0,
         };
      } else {
         ctx.throw(401, info);
         return;
      }
   })(ctx, next);
}

exports.post = loginLocalPassport;
