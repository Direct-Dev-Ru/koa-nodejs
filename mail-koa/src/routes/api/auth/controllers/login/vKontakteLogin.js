const passport = require('../../../../../libs/passport');
exports.oAuthVKontakte = (router) => {
   router.get(
      '/login/vkontakte',
      passport.authenticate('vkontakte', {
         scope: ['email'],
      })
   );

   router.get(
      '/oauth/vkontakte',
      passport.authenticate('vkontakte', {
         successRedirect: '/api/redirect',
         failureRedirect: '/',
         successFlash: true,
         failureFlash: true,
      })
   );
};
