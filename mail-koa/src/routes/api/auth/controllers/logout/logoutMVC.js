async function logout(ctx, next) {
   await ctx.logout();

   ctx.redirect('/');
}

exports.post = logout;
