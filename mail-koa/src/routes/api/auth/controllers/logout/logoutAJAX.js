async function logout(ctx, next) {
   await ctx.logout();

   if (!ctx.isAuthenticated()) {
      ctx.body = {
         isError: ctx.isAuthenticated(),
         message: {
            message: 'Logout success',
         },
         errorCode: 0,
      };

      ctx.redirect('/');
   } else {
      ctx.throw(500, 'During logout something went wrong ...');
      return;
   }
}

exports.post = logout;
