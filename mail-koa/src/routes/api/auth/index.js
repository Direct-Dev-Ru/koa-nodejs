const passport = require('../../../libs/passport');

const createAuthRoutes = (router, db, mw) => {
   const { validateObjectId, handleValidationError, mapUser } = mw;
   // MVC to main page
   router.get('/', require('../../index').get);

   // login routes
   router.post('/login', require('./controllers/login/loginLocalMVC').post);
   router.post('/auth/passport/locallogin', require('./controllers/login/loginLocalAjax').post);
   require('./controllers/login/vKontakteLogin').oAuthVKontakte(router);
   router.post('/redirect', require('./controllers/redirect/redirectMainSPA').post);

   // logout routes
   router.post('/auth/passport/logout', require('./controllers/logout/logoutAJAX').post);
   router.post('/logout', require('./controllers/logout/logoutMVC').post);

   // manual login logic
   // TODO JWT token add
   router.post('/auth/signin', async (ctx) => {
      const email = ctx.request.body.email;
      const password = ctx.request.body.password;
      const user = await db.users.findOne({ email }).populate('roles');

      let isValid = false;
      let message = '';
      if (!user) {
         message = `User with email = ${email} not found`;
      } else {
         isValid = await user.checkPassword(password);
         if (!isValid) {
            message = `Password incorrect`;
         } else {
            message = `Login success`;
         }
      }
      const { id, displayname } = mw.mapUser(user);
      ctx.body = {
         isError: false,
         message: {
            isValid,
            message,
            user: { id, displayname },
         },
         errorCode: 0,
      };
      return;
   });
};

module.exports = { createAuthRoutes };
