const { User } = require('../../../models/user/user.model');

const createUsersRoutes = (router, db, mw) => {
   const { validateObjectId, handleValidationError, mapUser } = mw;

   router.get('/users', async (ctx) => {
      console.log('is authenticated', ctx.isAuthenticated());
      console.log('session', ctx.session);

      const allUsers = await db.users.find().populate('roles').sort({ displayname: 1 }).lean();

      ctx.body = {
         isError: false,
         message: {
            users: allUsers.map(mapUser),
         },
         errorCode: 0,
      };
      return;
   });

   router.get('/users/:id', validateObjectId, async (ctx) => {
      const user = await db.users.findById(ctx.params.id);
      if (!user) {
         ctx.throw(404, `Not found: user with id ${ctx.params.id} not found`);
         return;
      }
      ctx.body = {
         isError: false,
         message: {
            users: [mapUser(user)],
         },
         errorCode: 0,
      };

      return;
   });

   router.get('/users/by/:field/:value', async (ctx) => {
      const field = ctx.params.field;
      const value = ctx.params.value;
      const filterObject = {};
      filterObject[field] = value;

      const users = await db.users.find(filterObject).populate('roles').sort({ name: 1 }).lean();

      if (users.length === 0) {
         ctx.throw(404, `Not found: users with ${field}=${value}`);
         return;
      }
      ctx.body = {
         isError: false,
         message: {
            users: users.map(mapUser),
         },
         errorCode: 0,
      };

      return;
   });

   router.delete('/users/:id', validateObjectId, async (ctx) => {
      const user = await db.users.findByIdAndRemove(ctx.params.id).populate('roles');
      if (!user) {
         ctx.throw(404, `Not found: user with id ${ctx.params.id} not found`);
         return;
      }
      ctx.body = {
         isError: false,
         message: {
            users: [mapUser(user)],
         },
         errorCode: 0,
      };
      //TODO: patch roles records - remove user id from their members list
      return;
   });

   // multy users insert
   router.post('/users', handleValidationError, async (ctx) => {
      let newusers = ctx.request.body.users;
      if (!newusers) {
         if (!ctx.request.user) {
            ctx.throw(400, `Not created: Users Array does not provided !`);
            return;
         }
         newusers = [ctx.request.user];
      }

      if (newusers.length === 0) {
         ctx.throw(400, `Not created: Users Array is empty !`);
         return;
      }

      const session = await db.mongoose.connection.startSession();
      session.startTransaction();
      console.log('transaction started');
      try {
         const users = [];

         for (const newuser of newusers) {
            //const user = await db.users.create([newuser], { session: session });
            const user = new User(newuser);
            await user.setPassword(newuser.password);
            await user.save({ session: session });
            console.log(user);
            users.push(user);
         }

         if (!users) {
            ctx.throw(500, `Not created: New Users Not Created`);
            return;
         }

         ctx.body = {
            isError: false,
            message: {
               users: users.map(mapUser),
            },
            errorCode: 0,
         };
         console.log(
            `${users.length} user created: ${users.map((u) => u.displayname).join(', ')} Now go to add roles ...`
         );
         await session.commitTransaction();
         console.log('transaction commited');
      } catch (err) {
         console.log(err);
         await session.abortTransaction();
         console.log('transaction aborted');
         throw err;
      } finally {
         console.log('session ended');
         session.endSession();
      }

      //TODO: patch roles records - add user id to their members list
   });

   router.patch('/users/:id', validateObjectId, handleValidationError, async (ctx) => {
      const email = ctx.request.body.email;
      const name = ctx.request.body.name;
      const roles = ctx.request.body.roles;
      //console.log(roles);
      const session = await db.mongoose.connection.startSession();
      session.startTransaction();
      console.log('transaction started');
      try {
         const user = await db.users.findByIdAndUpdate(
            ctx.params.id,
            {
               email,
               name,
            },
            {
               session: session,
               omitUndefined: true,
               new: true,
               runValidators: true,
            }
         );
         if (!user) {
            ctx.throw(404, `Not found: User with id ${ctx.params.id} not found`);
            return;
         }
         await session.commitTransaction();
         console.log('transaction commited');

         ctx.body = {
            isError: false,
            message: {
               users: [mapUser(user)],
            },
            errorCode: 0,
         };
      } catch (err) {
         await session.abortTransaction();
         console.log('transaction aborted');
         throw err;
      } finally {
         console.log('session ended');
         session.endSession();
      }
      //TODO: patch roles records - change user id from their members list
   });
};

module.exports = { createUsersRoutes };
