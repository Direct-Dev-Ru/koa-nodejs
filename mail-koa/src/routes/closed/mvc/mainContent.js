const { gp, ga } = global.app;
const { mw } = ga;

exports.addProtectedRoutes = (router, options) => {
   router.get('/protected/main-content', mw.checkAuthentication, function processProtectedRoute(ctx, next) {
      // console.log("full ctx:", JSON.stringify(ctx, null, 4));
      // console.log("is authenticated:", ctx.isAuthenticated());
      // console.log("session:", ctx.session);

      ctx.mainContent = {
         title: 'Основной контент сайта',
         author: ctx.state.user.email,
         content: 'Это защищенный раздел сайта. Никто не сможет просто так попасть сюда !!!',
      };
      // ctx.cookies.set("session", JSON.stringify(ctx.session), { httpOnly: true });
      ctx.body = ctx.render('content-main.pug');
   });
};
