exports.openRestfullRoutes = (router, options) => {
   //responds to /api/v2/testz
   router.get('/api/v2/testz', async function (ctx, next) {
      ctx.body = {
         isError: false,
         message: 'api is ready for you ...',
         errorCode: 0,
      };
      return;
   });

   //responds to /api/v2/healthz
   router.get('/api/v2/healthz', async function (ctx, next) {
      ctx.body = {
         isError: false,
         message: 'api (healthz route) is ready for you ...',
         errorCode: 0,
      };
      return;
   });
};
