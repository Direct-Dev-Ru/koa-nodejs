const { gp } = global.app;
const { logga } = global.app.ga;
const passport = require(gp.r('libs', 'passport'));
const { userSchema } = require(gp.r('models', 'user/user.model'));
const jwt = require('jsonwebtoken');
const key = global.app.config['TOKEN_KEY'];
const expirePeriod = +global.app.config['EXPIRATION'];

exports.afterAuth = async function (ctx, next) {
   if (ctx.isAuthenticated()) {
    //   logga('Объект ctx.state.user после успешного логина:', 'json');
      const userPO = userSchema.mapUser('main', ctx.state.user);

    //   logga(userPO, 'json');
      const payload = JSON.stringify({
         user: {
            id: userPO.id,
            email: userPO.email,
            displayname: userPO.displayname,
         },
         created: new Date(),
      });

      const now = new Date();
      const expDate = new Date(now.getTime() + expirePeriod);

      const token = jwt.sign({ exp: Math.floor(expDate - now / 1000), data: payload }, key);

      logga(token);

      ctx.cookies.set('usertoken', token, {
         httpOnly: false,
         expires: expDate,
      });

      ctx.redirect('/welcome');
   } else {
      logga('local authentication failed - redirect to start page');
      ctx.redirect('/');
   }
};
