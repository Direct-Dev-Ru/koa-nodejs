async function logout(ctx, next) {
   await ctx.logout();
   ctx.cookies.set('usertoken', '', { httpOnly: false });
   ctx.redirect('/');
}

exports.addRoutes = (router, options) => {
   router.post('/logout', logout);
   router.get('/logout', logout);
};
