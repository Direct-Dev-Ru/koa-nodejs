const { gp } = global.app;
const passport = require(gp.r('libs', 'passport'));

async function redirect(ctx, next) {
   console.log('full ctx:', JSON.stringify(ctx, null, 4));
   console.log('is authenticated:', ctx.isAuthenticated());
   console.log('session:', ctx.session);

   // check isAuthenticated
   // check user exists
   // generate jwt token expires 30sec
   // write jwt token to db to user
   ctx.cookies.set('session', JSON.stringify(ctx.session), { httpOnly: true });
   ctx.redirect(`http://localhost:3000/content/${ctx.session.passport.user}`);

   // after redirection spa should GET auth route with jwt to proof it
   // delete token from user in db
   // give new jwt token to user with life time n sec.
}

exports.addRoutes = (router, options) => {
   router.get('/redirect/main-spa', redirect);
};
