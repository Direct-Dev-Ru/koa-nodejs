const { gp } = global.app;
const { logga } = global.app.ga;
const passport = require(gp.r('libs', 'passport'));
const { userSchema } = require(gp.r('models', 'user/user.model'));
const jwt = require('jsonwebtoken');
const key = global.app.config['TOKEN_KEY'];
const expirePeriod = +global.app.config['EXPIRATION'];

exports.addRoutes = (router, options) => {
  router.get('/api/pretoken', async function (ctx, next) {
    const token = ctx.csrf || '000000';
    ctx.body = {
      isError: false,
      message: {
        token,
      },
      errorCode: 0,
    };
    return;
  });

  router.post(
    '/api/auth/signin',

    async function (ctx, next) {
      await passport.authenticate('local', async function (err, user, info) {
        if (err) throw err;

        if (user) {
          await ctx.login(user);

          if (ctx.isAuthenticated()) {
            const userDb = await ctx.db.users.findById(user.id);
            const userPO = userSchema.mapUser('main', ctx.state.user);

            // logga('Объект ctx.state.user после успешного логина:', 'json');
            // logga(userDb);
            // logga(userPO);


            const payload = JSON.stringify({
              user: {
                id: userPO.id,
                email: userPO.email,
                displayname: userPO.displayname,
              },
              created: new Date(),
            });

            const now = new Date();
            const expDate = new Date(now.getTime() + expirePeriod);

            const token = jwt.sign(
              {
                exp: Math.floor(expDate - now / 1000),
                data: payload,
              },
              key
            );

            //logga(token);

            ctx.cookies.set('usertoken', token, {
              httpOnly: false,
              expires: expDate,
            });

            ctx.body = {
              isError: false,
              message: {
                isValid: true,
                message: info,
                token,
                user: userPO,
              },
              errorCode: 0,
            };
          } else {
            // auth failed
            ctx.throw(401, info);
            return;
          }
        } else {
          ctx.throw(401, info);
          return;
        }
      })(ctx, next);
    }
  );
};
