const { gp } = global.app;
const passport = require(gp.r('libs', 'passport'));

exports.addRoutes = (router, options) => {
   router.get(
      '/login/github',
      passport.authenticate('github', {
         scope: ['user:email'],
      })
   );

   router.get(
      '/oauth/github',
      passport.authenticate('github', {
         successFlash: true,
         failureFlash: true,
      }),
      require('../controllers/localLoginAfter').afterAuth
   );
};
