const { gp } = global.app;
const passport = require(gp.r('libs', 'passport'));

exports.addRoutes = (router, options) => {
   router.post(
      '/loginlocal',
      passport.authenticate('local', {
         failureRedirect: '/login',
         failureFlash: true,
         successFlash: true,
      }),
      require('../controllers/localLoginAfter').afterAuth
   );
};

// exports.addRoutes = (router, options) => {
// 	router.post("/loginlocal", passport.authenticate("local", {
// 		successRedirect: "/",
// 		failureRedirect: "/",
// 		failureFlash: true, // ctx.flash()
// 		successFlash: true,
// 	}));
// };
