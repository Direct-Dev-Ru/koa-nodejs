const { gp } = global.app;
const passport = require(gp.r('libs', 'passport'));

exports.addRoutes = (router, options) => {
   router.get(
      '/login/vkontakte',
      passport.authenticate('vkontakte', {
         scope: ['email'],
      })
   );

   router.get(
      '/oauth/vkontakte',
      passport.authenticate('vkontakte', {
         successFlash: true,
         failureFlash: true,
      }),
      require('../controllers/localLoginAfter').afterAuth
   );
};
