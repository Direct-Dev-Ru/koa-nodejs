const Router = require('koa-router');
const { gp, ga } = global.app;
const { logga, mw } = ga;

const router = new Router();

const frontpage = async function (ctx, next) {
   if (ctx.isAuthenticated()) {
      // return !!ctx.state.user;
      // logga(ctx);
      ctx.body = ctx.render('welcome.pug');
   } else {
      // ctx.body = ctx.render("login.pug");
      ctx.body = ctx.render('start-page.pug');
   }
};
const loginpage = async function (ctx, next) {
   if (ctx.isAuthenticated()) {
      // return !!ctx.state.user;
      // logga(ctx);
      ctx.body = ctx.redirect('/welcome');
   } else {
      ctx.body = ctx.render('login.pug');
   }
};
const welcomepage = async function (ctx, next) {
   if (ctx.isAuthenticated()) {
      ctx.body = ctx.body = ctx.render('welcome.pug');
   } else {
      ctx.body = ctx.render('login.pug');
   }
};

/* GET home page. */
router.get('/', frontpage);
// login page
router.get('/login', loginpage);
// welcome page
router.get('/welcome', mw.checkAuthentication, welcomepage);

// login routes
require(gp.r('routes', 'opened/authentication/login/loginLocalMVC')).addRoutes(router);
// login from ajax
require(gp.r('routes', 'opened/authentication/login/loginLocalAjax')).addRoutes(router);

// oauth providers logins

// vkontakte
require(gp.r('routes', 'opened/authentication/login/vKontakteLogin')).addRoutes(router);
// github
require(gp.r('routes', 'opened/authentication/login/githubLogin')).addRoutes(router);

// logout
require(gp.r('routes', 'opened/authentication/logout/logoutMVC')).addRoutes(router);

// redirects
require(gp.r('routes', 'opened/authentication/redirect/redirectMainSPA.js')).addRoutes(router);

require(gp.r('routes', 'closed/mvc/mainContent')).addProtectedRoutes(router);

module.exports = router;
