const nodePath = process.argv[0];
const appPath = process.argv[1];
const secSettingsFile = process.argv[2] || '../secSettings';
const resultFile = process.argv[3] || '../encConfig.enc';
const { readSecretSync } = require('../../helpers/getSecret');
let secretKey = process.argv[4];

const path = require('path');
const { encryptJson, decryptJson, writeEncrypted, readEncrypted } = require('../../helpers/cypher');
let encrypted;
try {
   if (!secretKey) {
      secretKey = readSecretSync()?.value;
   }
   console.log("key:",secretKey);

   secSettingsPath = path.resolve(path.dirname(appPath), secSettingsFile);
   const secSettings = require(secSettingsPath);
   resultPath = path.resolve(path.dirname(appPath), resultFile);
   //    console.log('appPath : ', appPath);
   //    console.log('secSettingsPath : ', secSettingsPath);
   //    console.log('resultPath : ', resultPath);
   //    console.log('secSettings : ', secSettings);

   encrypted = writeEncrypted('encConfig.enc', secSettings, secretKey);
   console.log('after encrypted :', encrypted);
   //    control

   let decrypted = {};
   try {
      decrypted = readEncrypted('encConfig.enc', secretKey);
   } catch (error) {
      decrypted = { error };
   }
   console.log('after decrypted :', decrypted);
} catch (error) {
   console.log(error);
   encrypted = { error };
}
