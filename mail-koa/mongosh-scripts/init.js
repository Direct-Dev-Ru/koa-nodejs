db = connect(
	"mongodb://mailuser:mailpassword@mail-db:37017/mail?authSource=mail&replicaSet=rs0&readPreference=primary"
);
db.users.deleteMany({});
db.users.insertOne({
	name: "adminka",
	email: "info@direct-dev.ru",
});
db.users.insertOne({
	name: "mailmaster",
	email: "mailmaster@direct-dev.ru",
});
printjson(db.users.find());

// To run this file via mongosh
// mongosh --nodb --file init.js
