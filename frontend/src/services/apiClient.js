import axios from 'axios';

const defaultHeaders = {
   Accept: 'application/json',
   'Content-Type': 'application/json',
};

const defaultBaseURL = process.env.REACT_APP_API_BASEURL || 'http://localhost:3000/api';

const getAxiosClient = (
   headers = defaultHeaders,
   baseURL = defaultBaseURL,
   cred = true
) => {
   return axios.create({
      baseURL: baseURL,
      withCredentials: cred,
      headers: headers,
      validateStatus: (status) => {
         if (status === 401) {
            localStorage.removeItem('token');
            window.location.reload();
            return;
         }
         return status <= 399;
      },
   });
};

export default getAxiosClient;
