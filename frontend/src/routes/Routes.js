/* eslint-disable no-unused-vars */
import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { routes, URL_PREFIX } from './routesList';
const debug = process.env.NODE_ENV !== 'production';

debug && console.log('routes:', routes);

function intersect(a, b) {
   const setA = new Set(a);
   const setB = new Set(b);
   const intersection = new Set([...setA].filter((x) => setB.has(x)));
   return Array.from(intersection);
}

const calculateActualRoutes = (isAuth, user) => {
   return routes.filter((route) => {
      if (isAuth) {
         const rolesRequired = route?.roles ?? [];
         const rolesPresented = user?.roles ?? ['everyone'];
         //  debug && console.log('route:', route);
         //  debug && console.log('rolesRequired:', rolesRequired);
         //  debug && console.log('rolesPresented:', rolesPresented);

         if (!user) {
            return (
               rolesRequired.includes('everyone') &&
               !(route?.hideThenAuthed ?? false)
            );
         }
         const intersectOfRoles =
            intersect(rolesRequired, rolesPresented) ?? [];

         return (
            !(route?.hideThenAuthed ?? false) && intersectOfRoles.length > 0
         );
      }
      return route.protected === false;
   });
};

const getRoutesMap = (isAuth, user) => {
   const actualRoutes = calculateActualRoutes(isAuth, user);
   const routesMap = [];
   actualRoutes.forEach((route) => {
      if (route.hasOwnProperty('name')) {
         routesMap[route.name] = route.url;
      }
   });
   return routesMap;
};

const getRouteByName = (name) => {
   const route =
      routes.find((r) => r.name === name) ||
      routes.find((r) => r.name === '404');
   return route;
};

const getActualNamedRoutes = (isAuth, user, exclude = []) => {
   const actualRoutes = calculateActualRoutes(isAuth, user);
   const actualNamedRoutes = [];

   actualRoutes.forEach((route) => {
      // eslint-disable-next-line no-prototype-builtins
      if (route.hasOwnProperty('name') && !exclude.includes(route?.name)) {
         actualNamedRoutes.push({
            name: route.name,
            to: route.url,
            display: route.displayName,
            menuOrder: route?.menuOrder ?? -1,
         });
      }
   });
   return actualNamedRoutes.sort((a, b) => {
      const a_field = a?.menuOrder ?? -1;
      const b_field = b?.menuOrder ?? -1;
      let comparison = 0;
      if (a_field > b_field) {
         comparison = 1;
      } else if (a_field < b_field) {
         comparison = -1;
      }
      return comparison;
   });
};

const Routes = ({ isAuthenticated, user }) => {
   const actualRoutes = calculateActualRoutes(isAuthenticated, user);

   return (
      <>
         <Switch>
            {actualRoutes.map((route) => (
               <Route
                  path={route.url}
                  component={route.component}
                  exact={route.exact}
                  key={route.url}
               />
            ))}
            {/* <Redirect to={`${URL_PREFIX}/404`} /> */}
         </Switch>
      </>
   );
};

export { Routes, getRoutesMap, getActualNamedRoutes, getRouteByName };
