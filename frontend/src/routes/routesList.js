/* eslint-disable no-unused-vars */
import { Redirect } from 'react-router-dom';

import { Home } from '../components/Home';
import { SignIn } from '../components/SignIn';
import { SignUp } from '../components/SignUp';
import { Content } from '../components/Content';
import { E404 } from '../components/Errors/E404';

const debug = process.env.NODE_ENV !== 'production';

const SERVER_URL_PREFIX = process.env.REACT_APP_SERVER_URL_PREFIX || '';

const REACT_URL_PREFIX = process.env.REACT_APP_URL_PREFIX || '/';

const URL_PREFIX = SERVER_URL_PREFIX + REACT_URL_PREFIX;

debug && console.log('SERVER_URL_PREFIX', SERVER_URL_PREFIX);
debug && console.log('REACT_URL_PREFIX', REACT_URL_PREFIX);
debug && console.log('URL_PREFIX', URL_PREFIX);

const routes = [
   {
      name: 'home',
      displayName: 'Home',
      url: `/`,
      component: () => <Redirect to={`${URL_PREFIX}`} />,
      protected: false,
      roles: ['admin','reader','writer','everyone'],
      exact: true,
   },
   {
      name: 'home',
      displayName: 'Home',
      url: `${URL_PREFIX}`,
      component: Home,
      protected: false,
      roles: ['admin','reader','writer','everyone'],
      menuOrder: 0,
      exact: true,
   },
   {
      name: 'login',
      displayName: 'Sign In',
      url: `${URL_PREFIX}/signin`,
      component: SignIn,
      protected: false,
      hideThenAuthed: true,
      menuOrder: 1,
      roles: ['admin','everyone'],
      exact: true,
   },
   {
      name: 'logout',
      displayName: 'Sign Out',
      url: `${URL_PREFIX}/logout`,
      component: null, //SignOut,
      protected: true,
      roles: ['admin','everyone'],
      menuOrder: 100,
      exact: true,
   },
   {
      name: 'register',
      displayName: 'Sign Up',
      url: `${URL_PREFIX}/signup`,
      component: SignUp,
      protected: false,
      menuOrder: 2,
      hideThenAuthed: true,
      roles: ['admin','everyone'],
      exact: true,
   },
   {
      name: 'onenew',
      displayName: 'Новость',
      url: `${URL_PREFIX}/news/:id`,
      component: null,
      protected: true,
      menuOrder: -1,
      roles: ['admin', 'reader'],
      exact: true,
   },

   {
      name: 'allnew',
      displayName: 'Список новостей',
      url: `${URL_PREFIX}/news`,
      component: null,
      protected: true,
      menuOrder: 3,
      roles: ['admin', 'reader'],
      exact: true,
   },
   {
      name: 'maincontent',
      displayName: 'Основной контент',
      url: `${URL_PREFIX}/maincontent`,
      component: Content,
      protected: true,
      menuOrder: 4,
      roles: ['admin', 'everyone'],
      exact: true,
   },
   {
      name: '404',
      displayName: 'Page Not Found',
      url: `${URL_PREFIX}/404`,
      component: E404,
      protected: false,
      roles: ['admin', 'everyone'],
      exact: true,
   },
   {
     url: '*',
     component: E404,
     protected: false,
     menuOrder: -1,
     roles: ['everyone'],
   },
];

export { routes, URL_PREFIX };
