/* eslint-disable no-unused-vars */
import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js';
import './App.css';
import {
   BrowserRouter as Router,
   Switch,
   Route,
   Link as RouterLink,
} from 'react-router-dom';
import { AuthContext } from './context/AuthContext';
import { SignIn } from './components/SignIn';
import { SignUp } from './components/SignUp';
import { Content } from './components/Content';
import { Routes, getRouteByName, getActualNamedRoutes } from './routes/Routes';

const debug = process.env.NODE_ENV !== 'production';

function App() {
   const authContext = useContext(AuthContext);

   console.log(authContext);

   const { isAuthenticated, user } = authContext;
   debug && console.log('isAuthenticated, user :', isAuthenticated, user);

   const routesForMenu = getActualNamedRoutes(isAuthenticated, user, [
      '404',
      'home',
   ]).filter((item) => item?.menuOrder !== -1);
   
   debug && console.log('routesForMenu :', routesForMenu);

   return (
      <Router>
         <div className='App d-flex flex-column'>
            <header>
               {/* Hidden part */}
               <div className='collapse bg-dark' id='navbarHeader'>
                  <div className='container'>
                     <div className='row'>
                        <div className='col-sm-8 col-md-7 py-4'>
                           <h4 className='text-white'>About This Site</h4>
                           <p className='text-muted'>
                              This site is about news, todos and other things
                              ... Enjoy it!!!
                           </p>
                        </div>
                        <div className='col-sm-4 offset-md-1 py-4'>
                           <h4 className='text-white'>Menu:</h4>
                           <ul className='list-unstyled'>
                              <li>
                                 <RouterLink
                                    to={getRouteByName('home').url}
                                    className='text-white'
                                 >
                                    Home page
                                 </RouterLink>
                              </li>
                              <li>
                                 <RouterLink
                                    to={getRouteByName('login').url}
                                    className='text-white'
                                 >
                                    SignIn
                                 </RouterLink>
                              </li>
                              <li>
                                 <RouterLink
                                    to={getRouteByName('register').url}
                                    className='text-white'
                                 >
                                    SignUp
                                 </RouterLink>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>

               {/* Default visible part */}
               <div
                  className='navbar navbar-dark bg-dark shadow-sm'
                  style={{ height: '10vh' }}
               >
                  <div className='container'>
                     <RouterLink
                        to={getRouteByName('home').url}
                        className='navbar-brand d-flex align-items-center'
                     >
                        <strong>Direct-Dev-Ru</strong>
                     </RouterLink>
                     <div className='d-none d-md-flex justify-content-start' style={{width: '65%'}}>

                           {routesForMenu.map((route, idx) => (
                                 <RouterLink
                                    // className='nav-link text-white'
                                    key={route.name + idx.toString()}
                                    className='btn btn-outline-info m-1 p-2'
                                    aria-current='page'
                                    to={route.to}
                                 >
                                    {route.display}
                                 </RouterLink>
                              
                           ))}

                     </div>
                     <button
                        className='navbar-toggler'
                        type='button'
                        data-bs-toggle='collapse'
                        data-bs-target='#navbarHeader'
                        aria-controls='navbarHeader'
                        aria-expanded='false'
                        aria-label='Toggle navigation'
                     >
                        <span className='navbar-toggler-icon'></span>
                     </button>
                  </div>
               </div>
            </header>

            <main>
               <div className='contentWrapper d-flex flex-column justify-content-start'>
                  <Routes isAuthenticated={isAuthenticated} user={user} />
               </div>
            </main>
         </div>
      </Router>
   );
}

export default App;
