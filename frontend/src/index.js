/* eslint-disable no-unused-vars */
import React from 'react';
import ReactDOM from 'react-dom';
// import { useHistory } from 'react-router-dom';
import './index.css';
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import { AuthContextComponent } from './context/AuthContext';
import {  getRouteByName  } from './routes/Routes';
import App from './App';

ReactDOM.render(
  <AuthContextComponent>    
      <App />    
  </AuthContextComponent>,
  document.getElementById('root'),
);

