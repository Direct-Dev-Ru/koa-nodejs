/* eslint-disable no-unused-vars */
import React, { Component } from 'react';
import getAxiosClient from '../services/apiClient';
const AuthContext = React.createContext();
const AuthProvider = AuthContext.Provider;
const AuthConsumer = AuthContext.Consumer;

const apiClient = getAxiosClient();
const debug = process.env.NODE_ENV !== 'production';

class AuthContextComponent extends Component {
  constructor(props) {
    super(props);
    const initState = { isAuthenticated: false, user: {} };

    if (document?.cookie) {
      const cookieArray = (document?.cookie ?? '').split(';').map((cookie) => {
        const name = cookie.substring(0, cookie.indexOf('='));

        const value = cookie.substring(cookie.indexOf('=') + 1, 100000);
        return { name, value };
      });

      let cookieToken;
      if (cookieArray.length > 0) {
        const { value } = cookieArray.find(
          (cookie) => cookie.name === 'usertoken'
        );
        cookieToken = value;
      }

      const storageToken =
        sessionStorage.getItem('usertoken') ||
        localStorage.getItem('usertoken');
     
      if (storageToken === cookieToken) {
        initState.isAuthenticated = true;
        try {
          debug && console.log(storageToken);
          const user = JSON.parse(
            JSON.parse(atob(storageToken.split('.')[1])).data
          ).user;
          debug && console.log(user);
          initState.user = user;
          // {Authorization: `Bearer ${auth.token}`}

        } catch (e) {
          console.error(e);
        }
      }
    }
    this.state = initState;
  }

  setAuthenticated = (value) => {
    this.setState((prevState) => ({ ...prevState, isAuthenticated: value }));
  };
  setUser = (value) => {
    this.setState((prevState) => ({ ...prevState, user: value }));
  };
  setMultyFields = (value) => {
    this.setState((prevState) => ({ ...prevState, ...value }));
  };

  dispatchEvent = (actionType, payload) => {
    switch (actionType) {
      case 'SET_MULTY_FIELDS':
        this.setMultyFields(payload);
        return;
      case 'SET_USER':
        this.setUser(payload);
        return;
      case 'SET_AUTH':
        this.setAuthenticated(payload);
        return;
      case 'GET_LOGIN':
        return this.loginGen;
      default:
        return;
    }
  };

  loginGen = async ({
    type = 'local',
    endpoint = '/auth/signin',
    email = '',
    password = '',
  }) => {
    if (type === 'local') {
      try {
        const csrf = await apiClient.get('/pretoken');
        const csrfToken = csrf?.data?.message?.token || '000000';
        const body = { email, password, _csrf: csrfToken };
        const { data } = await apiClient.post(endpoint, body);
        return data;
      } catch (error) {
        console.log('ERROR POST LOGIN:', error);
        return error?.data || { isError: true, message: error };
      }
    } else if (type === 'vkontakte') {
      const { data } = await apiClient.get(endpoint);
      return data;
    }
  };

  render() {
    const { children } = this.props;
    return (
      <AuthProvider
        value={{
          ...this.state,
          dispatchEvent: this.dispatchEvent,
          apiClient,
        }}
      >
        {children}
      </AuthProvider>
    );
  }
}

export { AuthContextComponent, AuthContext, AuthProvider, AuthConsumer };
