/* eslint-disable func-names */
/* eslint-disable no-unused-vars */
import React from 'react';
import { Link as RouterLink, useHistory } from 'react-router-dom';
import { getRouteByName } from '../../routes/Routes';

function E404({
   title = 'Oooooops! Page not found !!!',
   message = 'Server said, there are no such page ...',
}) {
   //    const history = useHistory();

   return (
      <>
         <h1 style={{ fontSize: '4rem', color: 'red' }}>404</h1>
         <h2 style={{ fontSize: '2rem', color: 'red' }}>{title}</h2>
         <h2 style={{ fontSize: '2rem', color: 'red' }}>{message}</h2>

         <RouterLink
            className='btn btn-primary m-3'
            role='button'
            to={getRouteByName('home').url}
            style={{ color: 'white' }}
         >
            Go Home
         </RouterLink>
      </>
   );
}

export { E404 };
