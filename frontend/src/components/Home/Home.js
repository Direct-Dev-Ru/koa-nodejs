/* eslint-disable no-unused-vars */
import React from 'react';

import { Link as RouterLink } from 'react-router-dom';
import { getRouteByName } from '../../routes/Routes';
import { AuthContext } from '../../context/AuthContext';

const fakeNews = [
   {
      name: 'Доллар стал стоить 1 рубль',
      description:
         'Курс доллара установленный Ценральным банком РФ = 1 руб !!!',
      img: 'watafuck.jpg',
      path: '404',
   },
   {
      name: 'Воду можно заливать в бензобак ...',
      description:
         'Новые Лада Вотер ездят без бензина - просто добавть воды !!!!',
      img: 'Lada.jpg',
      path: '404',
   },
   {
      name: 'Белые медведи будут покрашены в бурый цвет',
      description:
         'Все белые медведи подлежат обязательной окраске в бурый цвет чтобы чукчи их видели издалека, такое решение принят Якутским парламетом',
      img: 'dontmissdeadline-min.jpg',
      path: '404',
   },
];

const Home = () => {
   const authContext = React.useContext(AuthContext);
   const { isAuthenticated, user, dispatchEvent } = authContext;
   const loginRoute = getRouteByName('login');
   const registerRoute = getRouteByName('register');
   return (
      <section className='py-5 text-center container'>
         <div className='row py-lg-5'>
            <div className='col-lg-6 col-md-8 mx-auto'>
               <h1 className='fw-light text-primary'>Latest News</h1>
               <p className='lead text-muted'>
                  Something short and leading about the collection below—its
                  contents, the creator, etc. Make it short and sweet, but not
                  too short so folks don’t simply skip over it entirely.
               </p>

               <RouterLink
                  className='btn btn-primary m-2'
                  role='button'
                  to={getRouteByName('login').url}
                  style={{ color: 'white', width: '45%' }}
               >
                  Sign In
               </RouterLink>

               <RouterLink
                  className='btn btn-secondary m-2'
                  role='button'
                  to={getRouteByName('register').url}
                  style={{ color: 'white', width: '45%' }}
               >
                  Sign Up
               </RouterLink>
            </div>
         </div>
      </section>
   );
};

export default Home;
