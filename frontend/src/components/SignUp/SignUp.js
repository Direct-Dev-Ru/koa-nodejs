import React, { Component } from 'react';
import styles from './SignUp.module.css';
import { getRouteByName } from '../../routes/Routes';
import { Link as RouterLink } from 'react-router-dom';
export default class SignUp extends Component {
   render() {
      return (
         <div className='auth-wrapper'>
            <div className='auth-inner'>
               <form>
                  <h3>Sign Up</h3>

                  <div className='form-group' style={{ marginTop: '5px' }}>
                     <label className={styles.label}>First name</label>
                     <input
                        type='text'
                        className='form-control'
                        placeholder='First name'
                     />
                  </div>

                  <div className='form-group'>
                     <label className={styles.label}>Last name</label>
                     <input
                        type='text'
                        className='form-control'
                        placeholder='Last name'
                     />
                  </div>

                  <div className='form-group'>
                     <label className={styles.label}>Email address</label>
                     <input
                        type='email'
                        className='form-control'
                        placeholder='Enter email'
                     />
                  </div>

                  <div className='form-group'>
                     <label className={styles.label}>Password</label>
                     <input
                        type='password'
                        className='form-control'
                        placeholder='Enter password'
                     />
                  </div>

                  <button
                     type='submit'
                     className={`btn btn-primary btn-block ${styles.btnSubmit}`}
                  >
                     Sign Up
                  </button>
                  <p className='forgot-password text-right'>
                     Already registered <RouterLink to={getRouteByName("login").url}>sign in?</RouterLink>
                  </p>
               </form>
            </div>
         </div>
      );
   }
}
