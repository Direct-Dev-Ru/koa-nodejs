/* eslint-disable no-unused-vars */
import React, { useState, useContext } from 'react';
import styles from './Content.module.css';
import { AuthContext } from '../../context/AuthContext';

const Content = ({ props }) => {
  const [localState, setLocalState] = useState({});
  const authContext = useContext(AuthContext);
  const { isAuth, user, apiClient } = authContext;
  console.log("Content rendering:",isAuth, user);

  const onTestApiCallClick = async () => {
    const { data } = await apiClient.get('/users');
    console.log(data);
  }

  return (
    <React.Fragment>

        <div className={styles.contentInner}>
          <h3>Content Part</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates dolor
            praesentium minus vitae dignissimos ratione. Nemo veritatis perferendis
            voluptates in, tempore cupiditate numquam iusto molestiae nisi nihil sint
            dolor, ad, quaerat eum delectus iure saepe. Reprehenderit impedit doloribus in
            sint modi ipsa nesciunt nemo! Autem voluptates eius voluptate doloremque.
            Quibusdam corporis debitis architecto facere illum labore necessitatibus
            veritatis excepturi ullam sunt? Veniam, obcaecati? Omnis quia a aliquam
            excepturi iste laudantium nihil. Ex et nobis excepturi? Sit illum nulla
            molestias enim placeat repudiandae ratione culpa a, sequi eos magni magnam
            facilis velit dolores rerum iure, obcaecati harum quo id ipsum quasi!
          </p>

          <button
              type="button"
              id="apiTestCall"
              name="api-test-call"
              className={`btn btn-secondary btn-block`}
              onClick={onTestApiCallClick}
            >
              Get All Users To Console
            </button>
        </div>
      
    </React.Fragment>
  );
};

export default Content;
