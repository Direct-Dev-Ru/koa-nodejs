/* eslint-disable no-unused-vars */
import React, { useState, useContext, useRef } from 'react';
import { useHistory } from 'react-router-dom';
import styles from './SignIn.module.css';
import { AuthContext } from '../../context/AuthContext';
import { getRouteByName } from '../../routes/Routes';

const debug = process.env.NODE_ENV !== 'production';

const SignIn = ({ props }) => {
  const history = useHistory();
  const [localState, setLocalState] = useState({
    email: '',
    password: '',
    error: null,
  });
  const authContext = useContext(AuthContext);
  const { isAuthenticated, user, dispatchEvent } = authContext;

  const remember = useRef();
  const onClick = () => {
    const loginFunc = dispatchEvent('GET_LOGIN');

    loginFunc({
      email: localState.email,
      password: localState.password,
      type: 'local',
      endpoint: '/auth/signin',
    }).then((data) => {
      if (data) {
        const { checked } = remember.current;
        debug && console.log(checked);
        if (!data.isError && data.message.isValid) {
          const { user, token } = data.message;
          debug && console.log('token from server', token);

          if (document?.cookie) {
            const cookieArray = document?.cookie.split(';').map((cookie) => {
              const name = cookie.substring(0, cookie.indexOf('='));

              const value = cookie.substring(cookie.indexOf('=') + 1, 100000);
              return { name, value };
            });

            let cookieToken;
            if (cookieArray.length > 0) {
              const { value } = cookieArray.find(
                (cookie) => cookie.name === 'usertoken'
              );
              cookieToken = value;
            }

            if (token === cookieToken) {
              if (checked) {
                localStorage.setItem('usertoken', token);
                sessionStorage.setItem('usertoken', '');
              } else {
                sessionStorage.setItem('usertoken', token);
                localStorage.setItem('usertoken', '');
              }

              dispatchEvent('SET_MULTY_FIELDS', {
                isAuthenticated: true,
                user,
              });
              history.push(getRouteByName('maincontent').url);
            } else {
              localStorage.setItem('usertoken', '');
              sessionStorage.setItem('usertoken', '');
              dispatchEvent('SET_MULTY_FIELDS', {
                isAuthenticated: false,
                user: {},
              });
            }
          } else {
            //   куков нет - пили на логин
            history.push(getRouteByName('login').url);
          }
        } else {
          debug && console.log(data);
          setError(data.message);
          dispatchEvent('SET_MULTY_FIELDS', {
            isAuthenticated: false,
            user: {},
          });

          localStorage.setItem('usertoken', '');
          sessionStorage.setItem('usertoken', '');
        }
      }
    });
  };

  const onClickVK = () => {
    // loginVk().then((data) => {
    //   console.log(data);
    // });
    window.location.href = 'http://localhost:3003/login/vkontakte';
  };
  const onClickGH = () => {
    // loginVk().then((data) => {
    //   console.log(data);
    // });
    window.location.href = 'http://localhost:3003/login/github';
  };

  const handleChange = (evt) => {
    setLocalState((prevState) => ({
      ...prevState,
      [evt.target.name]: evt.target.value,
    }));
  };

  const setError = (err) => {
    setLocalState((prevState) => ({ ...prevState, error: err }));
  };

  return (
    <React.Fragment>
      <div className='auth-wrapper'>
        <div className='auth-inner'>
          <form>
            <h3>Sign In</h3>
            {localState.error && (
              <p style={{ color: 'red', fontWeight: 'bold' }}>
                {localState.error?.message ?? 'Error occurs ...'}
              </p>
            )}
            <div className='form-group'>
              <label className={styles.label}>Email address: </label>
              <input                
                type='email'
                className='form-control'
                placeholder='Enter email ...'
                name='email'
                value={localState.email}
                onChange={handleChange}
              />
            </div>
            <div className='form-group'>
              <label className={styles.label}>Password:</label>
              <input
                type='password'
                className='form-control'
                placeholder='Enter password ...'
                name='password'
                value={localState.password}
                onChange={handleChange}
              />
            </div>
            <div className='form-group'>
              <div className='custom-control custom-checkbox'>
                <input
                  type='checkbox'
                  ref={remember}
                  className={`custom-control-input ${styles.m10}`}
                  id='customCheck1'
                />
                <label
                  className={`custom-control-label ${styles.m10}`}
                  htmlFor='customCheck1'
                >
                  Remember me
                </label>
              </div>
            </div>
            <div className='d-flex justify-content-around'>
              <button
                type='button'
                id='localLogin'
                name='local-login'
                className={`btn btn-primary btn-block ${styles.btnSubmit} m-2`}
                onClick={onClick}
              >
                Log In
              </button>
            </div>
            <p className='forgot-password text-right'>
              Forgot <a href='/forgot'>password?</a>
            </p>
            <div className='d-flex justify-content-around'>
              <button
                type='button'
                id='vkLogin'
                name='vk-login'
                className={`btn btn-secondary btn-block ${styles.btnOAuth} m-2`}
                onClick={onClickVK}
              >
                Vk Auth
              </button>
              <button
                type='button'
                id='vkGitHub'
                name='vk-login'
                className={`btn btn-secondary btn-block ${styles.btnOAuth} m-2`}
                onClick={onClickGH}
              >
                GitHub Auth
              </button>
            </div>
          </form>
        </div>
      </div>
    </React.Fragment>
  );
};

export default SignIn;
